API
***

Below is a description of the fw-curation API.


fw-curation
-----------

curator
^^^^^^^

.. automodule:: flywheel_gear_toolkit.utils.curator
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__

reporters
^^^^^^^^^
.. automodule:: flywheel_gear_toolkit.utils.reporters
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__

walker
^^^^^^

.. automodule:: flywheel_gear_toolkit.utils.walker
   :members:
   :undoc-members:
   :show-inheritance:

   .. automethod::__init__
