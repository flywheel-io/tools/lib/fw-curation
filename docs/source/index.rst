
fw-curation Documentation
=========================

fw-curation is a toolkit to make running custom curation scripts easier.
fw-curation requires at least ``python-3.8``, and can be install via ``pip`` 
with ``pip install fw-curation`` or ``poetry`` with 
``poetry add fw-curation``.  For more details see the :ref:`installation`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   fw_curation
   api
   release


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
