Release Notes
*************

0.1.2
=====

Maintenance
-----------

* Loosen SDK version requirement

0.1.1
======

* Initial Release
