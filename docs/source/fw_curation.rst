fw-curation
***********

.. _walker:

Walker
======
The :class:`fw_curation.walker.Walker` class provides a mechanism to
'walk' the flywheel hierarchy. The walker class can be instantiated from any container
(Group, Project, Subject, Session, Acquisition, Analysis) and will recursively walk
through all children, grand-children and so on.

The Walker class is primarily meant to be used with the HierarchyCurator class, the
combination of these allow custom actions to be taken on every container of the same
level under a given container.

An example is show below where the Walker is instantiated at the project level and
actions are taken on all subjects and all sessions in that project:

.. code-block:: python

    import flywheel
    import os
    from fw_curation.walker import Walker

    fw = flywheel.Client(os.environ.get('api_key'))
    proj = fw.lookup('testgroup/testproj')

    walker = Walker(proj)

    for container in walker.walk():
        if container.container_type == 'subject':
            # Do something with subjects
            pass
        elif container.container_type == 'sessions':
            # Do something with sessions
            pass
        else:
            continue

The walker has a number of configuration options to control whether or not each
container is reloaded, whether the walking is depth-first or breadth-first, and what
level (if any) to stop at.

Depth first vs. Breadth first:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default the Walker uses depth-first traversal, but can also be set to breadth-first traversal. 

NOTE: To see how to configure walker within the HierarchyCurator gear, please see the `documentation 
for the gear <https://gitlab.com/flywheel-io/flywheel-apps/hierarchy-curator#hierarchycurator>`_

For example if you have the following hierarchy structure:

.. code-block:: bash

    sub-01
    ├── ses-screening
    │   ├── acq-anat1
    │   │   └── T1w.dicom.zip
    │   └── acq-func1
    │       └── task1.dicom.zip
    └── ses-visit1
       ├── acq-anat1
       │   └── T1w.dicom.zip
       └── acq-func1
           └── task1.dicom.zip

Depth-first traversal would look like:

.. code-block:: bash

    sub-01                       1.
    ├── ses-screening            2. 
    │   ├── acq-anat1            3.
    │   │   └── T1w.dicom.zip    4. 
    │   └── acq-func1            5.
    │       └── task1.dicom.zip  6.
    └── ses-visit1               7.
       ├── acq-anat1             8.
       │   └── T1w.dicom.zip     9.
       └── acq-func1             10.
           └── task1.dicom.zip   11.

Whereas breadth-first traversal would look like:

.. code-block:: bash

    sub-01                       1.
    ├── ses-screening            2. 
    │   ├── acq-anat1            4.
    │   │   └── T1w.dicom.zip    8. 
    │   └── acq-func1            5.
    │       └── task1.dicom.zip  9.
    └── ses-visit1               3.
       ├── acq-anat1             6.
       │   └── T1w.dicom.zip     10.
       └── acq-func1             7.
           └── task1.dicom.zip   11.

.. _curator:

Curator
=======
The curator module provides two abstract classes for custom curation. These classes
are meant to be used with the walker class.

* :class:`flywheel_gear_toolkit.utils.curator.HierarchyCurator`: Used to curate an
  entire hierarchy:
    * The :class:`HierarchyCurator` class contains abstract methods to be implemented
      for curating all containers. If you do not wish to curate a container, then simply
      implement each `curate_<container>` method with `pass`.
    * Each container in the flywheel hierarchy has a corresponding `validate` and
      `curate` method in :class:`HierarchyCurator`. These methods should determine whether
      or not to curate each container, and how to curate, respectively. See the
      documentation on :class:`flywheel_gear_toolkit.utils.curator.HierarchyCurator` for
      more details.
    * The specific curator can be configured using the `self.config` dataclass which is
      an instance of :class:`flywheel_gear_toolkit.utils.curator.CuratorConfig`.
    * See more information on the `HierarchyCurator gear documentation <https://gitlab.com/flywheel-io/flywheel-apps/hierarchy-curator>`_
* :class:`flywheel_gear_toolkit.utils.curator.FileCurator`: Used to curate a single file:
    * The :class:`FileCurator` is very similar to :class:`HierarchyCurator` except it
      only contains methods for validating and curating files since it is only meant to be
      used at the file level.


Ex. 

.. code-block:: python

    import flywheel
    import os
    from flywheel_gear_toolkit.utils import walker, curator

    def classify_file(file):
        # Update classification to T1 structural if file name starts with struc_
        if file.name.startswith('struc_'):
            file.update_classification({'Intent':['Structural'], 'Measurement':['T1']})

    class myCurator(curator.HierarchyCurator):

        def curate_file(self, file_: flywheel.FileEntry):
            classify_file(file_)

    fw = flywheel.Client(os.environ.get('api_key'))

    proj = fw.lookup('group/project')

    my_walker = walker.Walker(proj)

    my_curator = myCurator()

    for container in my_walker.walk():
        my_curator.curate_container(container)


HierarchyCurator Config:
------------------------

In the HierarchyCurator gear, the HierachyCurator has a special attribute called ``self.config``
which carries an instance of the :class:`flywheel_gear_toolkit.utils.curator.CuratorConfig`.

This class has the following configuration values which are primarily used in the gear, but some
can also be used in standalone curation scripts.

* Multithreading configuration (only used in HierarchyCurator gear).
    * ``multi``: A Boolean value that specifies whether to run in multithreading or single-threaded mode. (Default True)
    * ``workers``: An integer number that specifies number of worker threads to use. (Default 1)
* Walking configuration (only used in HierarchyCurator gear).
    * ``depth_first``: Boolean value that results in depth-first (True) curation or breadth-first (False) curation.  (Default True).
    * ``reload``: Boolean value to either perform ``<container>.reload()`` on each container or not.  (Default False). 
    * ``stop_level``: Optional string container level to curate, but not queue any of its children.  (Default None)
    * ``callback``: An optional function which takes in a container and returns a Boolean.  If the function returns True for a given container, it's children will be queued, otherwise, it's children will NOT be queued.
* Reporting configuration (Can be used in standalone scripts.)
    * ``report``: Boolean opting in to reporting.  (Default False).
    * ``format``: Optional class containing the typed format of log records.  (Default :class:`flywheel_gear_toolkit.utils.reporters.LogRecord`)
    * ``path``: Optional ``pathlib.Path`` object of output path.  (Default ``Path("/flywheel/v0/output/output.csv")``).

Reporter
========

AggregatedReporter
------------------
Class for creating an aggregated reporter during curation. The aggregated reporter logs
errors and messages generated in the curator class to an output json or csv file.
The AggregatedReporter can be adjusted to provide various log formats depending on the
fields required.

The AggregatedReporter is particularly useful when one wants to report on many actions
being taken at multiple levels of the Flywheel hierarchy, such as in a curation gear.

General Use:
^^^^^^^^^^^^
Once the curator is instantiated, it has the method ``append_log`` which takes in
keyword arguments corresponding to the columns in a CSV or the Keys in a json blob.

The reporter can be used anywhere, but is meant for use within the
`HierarchyCurator gear <https://gitlab.com/flywheel-io/flywheel-apps/hierarchy-curator>`_.
The gear will instantiate a reporter for you if it is specified in the configuration options
as ``self.config.report = True``

Example usage in a standalone curation script

.. code-block:: python

    import os
    import pathlib
    import flywheel
    from flywheel_gear_toolkit.utils import curator, reporters, walker

    class Curator(curator.HierarchyCurator):

        def __init__(self, **kwargs):
            # Opt in to reporting, reporter will be instantiated automatically.
            self.config.report = True
            self.config.path = Path('./example.csv').resolve()
            super().__init__(**kwargs)

        def curate_session(self, session):
            try:
                raise ValueError      # something that may raise
            except Exception as exc:
                self.reporter.append_log(
                    container_type='session',
                    label=session.label,
                    container_id=session.id,
                    resolved=False,
                    err=str(exc)
                )

    fw = flywheel.Client(os.environ.get('api_key'))

    proj = fw.lookup('group/project')

    my_walker = walker.Walker(proj)

    my_curator = Curator()

    for container in my_walker.walk():
        my_curator.curate_container(container)

Using a different custom logging format:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
By default the :class:`AggregatedReporter` uses the format from :class:`LogRecord`, but
it is easy to change this default formatting:

You can create a custom class that adds other fields or utilizes it's own format.

Subclass the :class:`flywheel_gear_toolkit.utils.reporters.BaseLogRecord` class with
the decorator for dataclass and add the fields you want with type annotations:

.. code-block:: python

    import dataclasses
    from flywheel_gear_toolkit.utils import reporters

    @dataclasses.dataclass
    class LogRecord(reporters.BaseLogRecord):
        c_type: str = ""
        c_id: str = ""
        label: str = ""
        err: str = ""
        msg: str = ""
        resolved: str = "False"
        search_key: str = ""

    reporter = reporters.AggregatedReporter(
        output_path="example.json",
        format=LogRecord
    )

.. _install-requirements:

install_requirements
====================
The ``fw-curation`` package provides the ``install_requirements()`` method which allows
for programatically installing requirements.

Generally it is bad practice to programatically install requirements, as whatever the
gear needs to run should be included in the docker container (i.e. installed via `pip`
in the Dockerfile), however there are some use cases where it may be unavoidable, and
this module helps provide that functionality.

ex.

.. code-block:: python

    from flywheel_gear_toolkit import GearToolkitContext
    from fw_curation import install_requirements
    import logging
    from pathlib import Path

    log = logging.getLogger(__name__)
    with GearToolkitContext() as gc:
        gc.init_logging()
        req_path = Path(gc.get_input_path('requirements'))
        try:
            install_requirements(req_path)
        except SystemExit as e:
            log.error('Could not install requirements')


