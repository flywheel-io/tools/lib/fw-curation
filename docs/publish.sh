#!/usr/bin/bash
if [ -d public ]; then
    rm -r public
fi

cd docs || exit
poetry run make html
mv build/html ../public
