# fw-curation

## Introduction

`fw-curation` is a python package maintained by [Flywheel](https://flywheel.io)
It provides a set of utilities for writing SDK scripts to perform curation.

This library allows a user to:

* Walk the Flywheel Hierachy (in a configurable manner) And
* Run custom code on each container (referred to as curation)

## Installation

The package can be installed using pip or poetry using python 3.8 or later.

```bash
    pip install fw-curation
    # or
    poetry add fw-curation
```

## License

`fw-curation` is developed under an MIT-based license.
