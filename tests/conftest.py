from pathlib import Path

pytest_plugins = ("flywheel_gear_toolkit.testing",)

ASSETS_DIR = Path(__file__).parents[0] / "data"
